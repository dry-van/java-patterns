package singleton;

public class A1 {
	private static int counter=0;
	private static A1 instance=null;
		private A1(){
			counter++;
			System.out.println("�ounter "+counter);
		}
	public static A1 getInstance(){
		if (instance==null){
			instance=new A1();
		}
		return instance;
	}
}
